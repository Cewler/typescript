export class TuplesExample {

    /**
     * A basic 2-value tuple from old Typescript
     */
    static basicTuple(): void {
        type KeyValuePair = [String, Number];

        let mapEntry: KeyValuePair = ['test', 1];
        // let invalidMapEntry: KeyValuePair = ['test', 'test2'];

        console.log('0: ' + mapEntry[0]);
        console.log('1: ' + mapEntry[1]);
        console.log(JSON.stringify(mapEntry));

        // mapEntry[0].charAt(0)
        // mapEntry[1].charAt(1);
    }

    /**
     * Tuple methods can have specifically set argument types.
     * They can then be called only with these arguments (in correct order).
     */
    static callTupleMethod() {
        TuplesExample.tupleMethod(1, 'test', 1);

        let someTuple: [number, string, number] = [1, 'test', 1];
        TuplesExample.tupleMethod(...someTuple);
        TuplesExample.equivalentTupleMethod(...someTuple);

        let pointTuple: [number, number, number] = [1, 2, 3];
        TuplesExample.tuplePointMethod(pointTuple);
    }

    static tupleMethod(...args: [number, string, number]) {
        console.log(JSON.stringify(args));
    }

    static equivalentTupleMethod(arg1: number, arg2: string, arg3: number) {

    }

    static tuplePointMethod(point: Point3D) {
        console.log(JSON.stringify(point));
    }

    /**
     * Tuples can have optional arguments
     */
    static optionalTupleArguments() {
        type OptionalTuple = [string, number?];
        let incomplete: OptionalTuple = ['test'];
        let complete: OptionalTuple = ['test', 1];

        type LongOptionalTuple = [string, number?, number?, number?];
        let incompleteLong: LongOptionalTuple = ['test', 1, 1, 1];
        incompleteLong = incomplete;
        // incomplete = incompleteLong;
    }

    /**
     * We can create tuple interfaces
     */
    static tupleInterface() {
        let tupleImpl: ITuple;
        tupleImpl = ['test', 1, 2];
    }
}

export type Point3D = [number, number, number];

export interface ITuple {
    0: string,
    1: number,
    2: number
}