export class ConstAssertionsExample {

	private EXAMPLE_CONSTANT = 1 as const; // automatically readonly
	private EXAMPLE_CONSTANT_2 = <const>1; // automatically readonly


	codeUsage() {
		let a = 'b' as const;
		// a = 'c';

		// let person = new Person() as const;
	}

}

class Person {
	name?: string
}
