export class ConstantNamedPropertiesExample {

    /**
     * We can use constants (value or symbol) to define properties on objects
     */
    basicExample() {
        let randomObject = {
            [NAME]: 'test',
            [DATE_CREATED]: new Date()
        };
        let a = randomObject[NAME];
        let b = randomObject[DATE_CREATED];

        let c: ConstantNamedPropertiesInterface = {[MEGA_PROPERTY]: 'test'};
        c[MEGA_PROPERTY].charAt(1);
    }
}

export const NAME = 'name';
export const DATE_CREATED = 'created';
export const MEGA_PROPERTY = Symbol('mega');

export interface ConstantNamedPropertiesInterface {
    [MEGA_PROPERTY]: string;
}