export class ImprovedTypeInferenceExample {

	basicExample() {
		let condition = false;
		let object = condition ? {a: 1, b: 2} : {};
		object.a; // a inferred to number | undefined. In older Typescript versions this would throw an exception because object doesn't have a property.

		let objectWithDoubleType = condition ? {a: 1} : {a: 'ONE'};
		objectWithDoubleType.a // a inferred to number | string
	}
}
