export class BigIntExample {

	someValue: bigint = 10000000000000000000000000000000000000000000000000000000000000000000000n;
	someOtherValue: BigInt = 10000000000000000000000000000000000000000000000000000000000000000000000000000000n;

	numberBigIntIncompatibility() {
		let numberVar = 1;
		let bigintVar = 1000n;

		// numberVar = bigint;
		// bigintVar = numberVar;
		bigintVar = BigInt(numberVar); // works great
		numberVar = Number(bigintVar); // works, but makes no sense since bigints are bigger

		// let sum = bigintVar + numberVar;
	}

	maxNumber() {
		let numberVar = Number.MAX_SAFE_INTEGER;
		console.log(`Number: 
			${numberVar},
			${numberVar + 1},
			Bad equality: ${numberVar + 1 === numberVar + 2}
		`);

		let bigintVar = BigInt(Number.MAX_SAFE_INTEGER)
		console.log(`BigInt: 
			${bigintVar}
			${bigintVar + 1n}
			Bad equality: ${bigintVar + 1n === bigintVar + 2n}
		`);
	}
}
