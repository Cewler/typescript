export class TopLevelAwaitExample {

	async fetchMeSomething(): Promise<string> {
		return '';
	}

	normalFunction() {
		// const fetchResult: string = await this.fetchMeSomething();
	}

	async asyncFunction(): Promise<void> {
		const fetchResult: string = await this.fetchMeSomething();
	}
}

const topLevelAwaitExample = new TopLevelAwaitExample();
// const fetchResult: string = await optionalChainingExample.fetchMeSomething(); // esnext or system
