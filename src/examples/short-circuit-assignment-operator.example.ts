export class ShortCircuitAssignmentOperatorExample {

	example() {
		let a = true;
		let b = false;

		a &&= b; // a = a && b;
		a ||= b; // a = a || b;
		a ??= b; // a = a ?? b;
	}

	methodWithNonNullArg(values: string[]) {
		(values ??= []).push('any');
		return values;
	}
}


