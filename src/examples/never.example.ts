export class NeverExample {

    /**
     * A never method cannot reach an end point
     */
    neverMethod(message: string): never {
        console.log('Error', message);
        throw new Error('Something went wrong: ' + message);
    }

    /**
     * It can also be a never-ending loop
     */
    neverEndingMethod(): never {
        while (true) {
            console.log('Do smthng');
            
        }
    }
}