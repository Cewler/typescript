/**
 * A type is considered weak if all of its properties are optional.
 */
export class WeakTypeDetectionExample {

    /**
     * It's now an error to assign anything to a weak type when there's no overlap in properties
     */
    basicExample() {
        let halfPerson = {name: 'Marek'};
        this.markPerson(halfPerson);

        let fullPerson = {name: 'Marek', id: 1};
        this.markPerson(fullPerson);

        let wrongAttributeTypes = {name: 1, id: 'test'};
        // this.markPerson(wrongAttributeTypes);

        let nonPerson = {address: 'Tomasikova'};
        // this.markPerson(nonPerson);
    }

    markPerson(person: Person) {

    }
}

interface Person {
    name?: string,
    id?: number
}