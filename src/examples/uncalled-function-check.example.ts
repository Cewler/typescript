export class UncalledFunctionCheckExample {

	deleteSomethingVeryImportant(caller: User) {
		// if (caller.isAdministrator) {
			// just do it
		// }
	}

	// Ternary check added in Typescript 3.9
	// getVeryImportantData(caller: User): string {
	// 	return caller.isAdministrator ? 'result' : null;
	// }

}

interface User {
	isAdministrator(): boolean;
	notify(): void;
	doNotDisturb?(): boolean;
}
