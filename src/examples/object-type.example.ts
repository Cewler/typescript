export class ObjectTypeExample {

    objectMethodsExample() {
        // Object.create(1);
        Object.create(new Number(1)).constructor;

        let a: Object = new Number(1);
        let b: object = new Number(1);
        // let c: object = 1;
        let d: Object = 1;
    }

    emptyObjectExample() {
        let a = {};
    }

    weakMapExample() {
        let map: WeakMap<Number, any> = new WeakMap();

    }

}

type Primitive =
    | boolean
    | number
    | string
    | symbol
    | null
    | undefined;

type NonPrimitive = object;