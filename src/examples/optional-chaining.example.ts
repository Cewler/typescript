export class OptionalChainingExample {

	example() {
		let person: Person = {
			firstName: 'a',
			lastName: 'b',
			age: 1
		};

		console.log(`Person middle name: ${person?.middleName}`);

		if (person?.middleName) {
			console.log(`Middle name: ${person.middleName}`);
		}

		if (person?.child?.firstName) {
			console.log(`Child name ${person.child.firstName}`);
		}

		if (person?.age) {
			console.log(`Optional age is valid ${person?.age}`);
		}
		if (person && person.age) {
			console.log(`Age boolean check is not valid`);
		}
	}

	optionalMethod(method?: () => void) {
		// method?.();
	}

}

type Person = {
	firstName: string,
	middleName?: string,
	lastName: string,
	age: number,
	child?: Person
}
