export class LookupTypesExample {

	getProperty<T, K extends keyof T>(obj: T, key: K) {
		return obj[key];
	}

	keyofExample() {
		let person: Person = {name: 'Marek', id: 1, birthDate: new Date()};
		this.getProperty(person, "id").toPrecision(1);
		this.getProperty(person, "name").charAt(1);
		this.getProperty(person, "birthDate").getDate();
		// this.getProperty(person, "noprop");

		person["id"].toPrecision(1);
		person["name"].charAt(1);
		person["birthDate"].getDate();
		// person["noprop"];
	}

	/**
	 * Since Typescript version 2.9, keyof now supports numbers and symbols as named properties
	 */
	keyOf2_9Example() {
		const enum E1 {A, B, C}

		const symbolProperty = Symbol();

		let person = {
			name: 'Jason',
			id: 2,
			[1]: 1,
			[E1.A]: 'test',
			[symbolProperty]: new Date()
		};

		this.getProperty(person, E1.A).charAt(1);
		this.getProperty(person, 1).toPrecision(1);
		this.getProperty(person, symbolProperty).getDate();

	}


	partialExample() {
		// let fullPerson: Person = {name: 'Marek', id: 1};
		// let fullPerson: Person = {name: 'Marek'} as Person;

		let partialPerson: Partial<Person> = {name: 'Marek'};
		console.log(`Partial: ${partialPerson.name} ${partialPerson.id}`);
	}

	readOnlyExample() {
		let readonlyPerson: Readonly<Person> = {name: 'Marek', id: 1, birthDate: new Date()};
		// readonlyPerson.name = 'test';
	}

	pickExample() {
		let person: Person = {name: 'Marek', id: 1, birthDate: new Date()};
		let pickedProps: Pick<Person, 'name' | 'id'> = {name: 'Marek', id: 1};
		// let pickedProps: Pick<Person, 'name' | 'id'> = {name: 'Marek', id: 1, birthDate: new Date()};
	}

	recordExample() {
		type OldSettings = { [pref: string]: any; }

		type SettingName = "WEB_URL" | "API_URL";
		type RecordSettings = Record<SettingName, any>;

		const oldSettings: OldSettings = {
			hocico: 'test',
			hocico2: 'test2'
		};
		const newSettings: RecordSettings = {
			API_URL: 'localhost',
			WEB_URL: 'test'
		};

		const partialSettings: Partial<RecordSettings> = {
			WEB_URL: 'test'
		}
	}
}

interface Person {
	name: string,
	id: number,
	birthDate: Date;
}

interface PartialPerson {
	name?: string,
	id?: number,
	birthDate?: Date
}