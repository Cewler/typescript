export class FunctionPropertiesExample {

	/**
	 * Functions can now also have properties and methods defined,
	 * so they no longer behave as only functions, but also as classes.
	 */
	basicExample() {
		function hackSomething(pc: string) {
			console.log('Just doing light hacking.');
		}

		hackSomething.destroy = (pc: string) => {
			console.log(`Destroying PC ${pc}`);
		};
		hackSomething.success = true;

		hackSomething('mmitro-PC');
		hackSomething.destroy('mmitro-PC');
		console.log(`Hack successful: ${hackSomething.success}`);
	}

}