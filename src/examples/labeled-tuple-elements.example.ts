type Tuple = [string, string];
type LabeledTuple = [firstName: string, lastName: string]; // IDEA issue, but works

type PersonName = [firstName: string, lastName: string] | [firstName: string, middleName: string, lastName: string]

export class LabeledTupleElementsExample {

	httpGet(...args: [string, number]) { // old way
		// do a http call with those args

		// same as httpGet(arg0: string, arg1: number)
	}

	httpPost(...args: [orderId: number, updatedValue: string]) {

	}

	createPerson(...name: PersonName) {

	}

	callCreatePerson() {
		this.createPerson('John', 'Doe');
		this.createPerson('John', 'Kevin', 'Doe');
	}
}


