export class UnknownExample {

    anything: any = 'test';
    nothing: unknown = 'test';

    /**
     * Any can be assigned to other objects,
     * but Unknown cannot.
     *
     * You would need to check its type first.
     */
    assignTypes(): void {
        let num: number;
        let text: string;

        // num = this.anything;
        // text = this.anything;
        // num = this.nothing;
        // text = this.nothing;

        // if (typeof this.nothing === "string") {
        //     text = this.nothing;
        // }
    }

    /**
     * You can call methods on an Any type, but
     * cannot call them on an Unknown type without checking its type first.
     */
    callMethods(): void {
        // this.nothing.charAt(0);
        if (this.nothing instanceof String) {
            console.log('Test', this.nothing.charAt(0));
        } else {
            console.log('Not an instanceof string');
        }

        // this.nothing.literallyAnyMethod();
        // this.anything.literallyAnyMethod();
    }

}