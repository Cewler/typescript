/**
 * Definite assignment check in strict mode (--strictPropertyInitialization)
 * gives you an error if all
 * non-nullable properties are not initialized in constructor or
 * when defining the property.
 **/
export class DefiniteAssignmentExample {
	a!: string;
	b!: string;

	variableInitializedExample() {
		let x!: number;
		// let x: number;
		this.initialize();
		this.doSomething(x);
	}

	nonNullAssertionExample() {
		let x: number;
		this.initialize();
		this.doSomething(x!);
	}

	doSomething(x: number) {
	}

	initialize() {
		// let's just say I'm initializing something here
	}
}

class DefinedProperties {
	a: string;
	b: string;

	constructor() {
		this.a = 'A';
		this.b = 'B';
	}
}

// class MissingConstructorDefinition {
//     a: string;
//     b: string;
//
//     constructor() {
//         this.a = 'A';
//     }
// }

// class MissingPropertyDefinition {
//     a: string = 'A';
//     b: string;
// }

class UndefinedProperty {

	a: string;
	b: string | undefined;
	c?: string;

	constructor() {
		this.a = 'A';
	}
}