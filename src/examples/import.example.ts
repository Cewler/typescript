import {Point3D} from "./tuples.example";

export class ImportExample {

    /**
     * The old import was using import at the top of the class
     */
    oldImport(point: Point3D) {
    }

    /**
     * New import type can import the class in the function
     */
    newImport(tuple: import("./tuples.example").ITuple) {

    }
}