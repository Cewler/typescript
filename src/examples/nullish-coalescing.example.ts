export class NullishCoalescingExample {

	example() {
		const zero = 0;
		const zeroOrTwo = zero || 2;
		const zeroCoalesceTwo = zero ?? 2;

		console.log(`Results: ${zeroOrTwo} / ${zeroCoalesceTwo}`);
	}

}
