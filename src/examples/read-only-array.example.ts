export class ReadOnlyArrayExample {

	before() {
		const readOnlyArray: ReadonlyArray<number> = [1, 2, 3];


	}

	now() {
		const readOnlyArray: readonly number[] = [1, 2, 3];
		const normalArray: number[] = [1, 2, 3];

		// normalarray.push(4);
		// roarray.push(4);
	}

	tuple() {
		const normalTuple: [string, number] = ['a', 1];
		normalTuple.push('b', 4);

		const readOnlyTuple: readonly [string, number] = ['a', 1];
		// readOnlyTuple.push('b', 2);
	}
}
