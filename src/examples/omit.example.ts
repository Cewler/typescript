export class OmitExample {

	test() {
		let person = {firstName: 'a', middleName: 'b', lastName: 'c'} as Person;
		// let person = {firstName: 'a', middleName: 'b', lastName: 'c'} as PersonWithoutAMiddleName;
		console.log(person.middleName);
	}

}

type Person = {
	firstName: string,
	middleName: string,
	lastName: string
}

type PersonWithoutAMiddleName = Omit<Person, 'middleName'>;
