export class StringEnumsExample {

    printEnums() {
        console.log(`Basic enum: ${Object.keys(BasicEnum)}`);
        console.log(`Number enum: ${Object.keys(NumberEnum)}`);
        console.log(`String enum: ${Object.keys(StringEnum)}`);

        console.log(`Reverse mapped basic: ${BasicEnum.FIRST}: ${BasicEnum[BasicEnum.FIRST]}`);
        console.log(`Reverse mapped string is the same: ${StringEnum.FIRST}: ${StringEnum[StringEnum.FIRST]}`)
    }
}

enum BasicEnum {
    FIRST,
    SECOND
}

enum NumberEnum {
    FIRST = 1,
    SECOND = 2
}

enum StringEnum {
    FIRST = "FIRST",
    SECOND = "SECOND"
}