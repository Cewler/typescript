export class GenericParameterDefaultsExample {

    genericExample() {
        let normalGeneric: NonDefaultGeneric<string> = {attribute: 'test'};
        // let normalGeneric2: NonDefaultGeneric = {attribute: 'test'};

        let defaultGeneric: DefaultGeneric = {attribute: 'test'};
        // let defaultGeneric: DefaultGeneric = {attribute: 1};

        let complexGeneric: ComplexDefaultGeneric = {att1: 'test', att2: 'test', att3: 1};
        let complexGeneric2: ComplexDefaultGeneric<number> = {att1: 1, att2: 'test', att3: 1};
        let complexGeneric3: ComplexDefaultGeneric<number, number> = {att1: 1, att2: 2, att3: 3};
    }

}

interface NonDefaultGeneric<T> {
    attribute: T;
}

interface DefaultGeneric<T = string> {
    attribute: T;
}

interface ComplexDefaultGeneric<T = string, K = string, V = number> {
    att1: T,
    att2: K,
    att3: V
}