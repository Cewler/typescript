/**
 * Not in ECMAScript yet, maybe it will be in ECMAScript2019
 */
export class OptionalCatchClauseVariablesExample {

    /**
     * 2 use cases:
     *  - If you want to completely ignore the error.
     *  - You don’t care about the error or you already know what it will be, but you do want to react to it.
     */
    basicExample() {
        try {
            throw 'some random error';
        } catch (exception) {
            console.error(exception);
        }

        try {
            throw 'some random error';
        } catch {

        }
    }

    parseJson() {
        let jsonData;
        try {
            jsonData = JSON.parse('non valid json')
        } catch {
            jsonData = {value: 'default'};
        }
    }

    logError(err: Error) {
        try {
            console.error(err);
        } catch {
            // Nothing we can do about it
        }
    }

}