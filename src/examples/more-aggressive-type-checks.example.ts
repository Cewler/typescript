export class MoreAggressiveTypeChecksExample {

	// Invalid types are reduced to never
	createInvalidType() {
		type RoundSquare = Circle & Square;
		// const roundSquare: RoundSquare = {};
	}

	// Types extending any do not act as any
	doAnything<T extends any>(tArg: T, anyArg: any) {
		console.log(anyArg.thisDoesNotExist);
		// console.log(tArg.thisDoesNotExist);
	}
}

interface Circle {
	kind: "circle";
	radius: number;
}

interface Square {
	kind: "square";
	sideLength: number;
}
