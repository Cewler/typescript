export class ClassInferenceFromConstructorExample {

	// Before they were any
	name;
	value;
	// initializedValue;

	constructor() {
		this.name = 'Johnny';
		this.value = 1;
		this.init();
	}

	private init() {
		// this.initializedValue = 5;
	}

	workWithProperties() {
		this.name.toLowerCase()
		// this.value.toLowerCase();

		this.value.toExponential();


		// this.initializedValue.toExponential();
		// this.initializedValue.toLowerCase();
	}
}


