export class ConditionalTypesExample { // TODO

    createNewType() {
        let numberOfLegs: Dog extends Animal ? Number : String;
        // numberOfLegs = 1;
        // numberOfLegs = '1';
    }

    inferType(arg: number | any) {
        type IsNumber<T> = T extends number ? true : false;
        type numberIsNumber = IsNumber<3>;
        type anyIsNumber = IsNumber<''>;
    }

}

interface Animal {
    legs: number;
}

interface Dog extends Animal {

}

interface Flower {
    legs: string;
}