import {UnknownExample} from "./examples/unknown.example";
import {TuplesExample} from "./examples/tuples.example";
import {ImportExample} from "./examples/import.example";
import {NeverExample} from "./examples/never.example";
import {LookupTypesExample} from "./examples/lookup-types.example";
import {ObjectTypeExample} from "./examples/object-type.example";
import {GenericParameterDefaultsExample} from "./examples/generic-parameter-defaults.example";
import {WeakTypeDetectionExample} from "./examples/weak-type-detection.example";
import {StringEnumsExample} from "./examples/string-enums.example";
import {OptionalCatchClauseVariablesExample} from "./examples/optional-catch-clause-variables.example";
import {NumericSeparatorsExample} from "./examples/numeric-separators.example";
import {ConstantNamedPropertiesExample} from "./examples/constant-named-properties.example";
import {ConditionalTypesExample} from "./examples/conditional-types.example";
import {DefiniteAssignmentExample} from "./examples/definite-assignment.example";
import {FunctionPropertiesExample} from "./examples/function-properties.example";
import {BigIntExample} from "./examples/big-int.example";
import {ReadOnlyArrayExample} from "./examples/read-only-array.example";
import {ConstAssertionsExample} from "./examples/const-assertions.example";
import {OmitExample} from "./examples/omit.example";
import {OptionalChainingExample} from "./examples/optional-chaining.example";
import {NullishCoalescingExample} from "./examples/nullish-coalescing.example";
import {TopLevelAwaitExample} from "./examples/top-level-await.example";
import {UncalledFunctionCheckExample} from "./examples/uncalled-function-check.example";
import {MoreAggressiveTypeChecksExample} from "./examples/more-aggressive-type-checks.example";
import {LabeledTupleElementsExample} from "./examples/labeled-tuple-elements.example";
import {ClassInferenceFromConstructorExample} from "./examples/class-inference-from-constructor.example";
import {ShortCircuitAssignmentOperatorExample} from "./examples/short-circuit-assignment-operator.example";

// --- Typescript 4.0 ---
const labeledTupleElementsExample = new LabeledTupleElementsExample();
const classInferenceFromConstructorExample = new ClassInferenceFromConstructorExample();
const shortCircuitAssignmentOperatorExample = new ShortCircuitAssignmentOperatorExample();

// --- Typescript 3.9 ---
const moreAggressiveTypeChecksExample = new MoreAggressiveTypeChecksExample();

// --- Typescript 3.8 ---
const topLevelAwaitExample = new TopLevelAwaitExample();

// --- Typescript 3.7 ---
const optionalChainingExample = new OptionalChainingExample();
const nullishCoalescingExample = new NullishCoalescingExample();
const uncalledFunctionCheckExample = new UncalledFunctionCheckExample();

// --- Typescript 3.5 ---
const omitExample = new OmitExample();

// --- Typescript 3.4 ---
const readOnlyArrayExample = new ReadOnlyArrayExample();
const constAssertionsExample = new ConstAssertionsExample();

// --- Typescript 3.3 ---

// --- Typescript 3.2 ---
const bigIntExample = new BigIntExample();

// --- Typescript 3.1 ---
// Properties declarations on functions
const functionPropertiesExample = new FunctionPropertiesExample();

// --- Typescript 3.0 ---
const unknownExample = new UnknownExample();
const tuplesExample = new TuplesExample(); // Tuples in rest parameters and spread expressions

// --- Typescript 2.9 ---
const importExample = new ImportExample();

// --- Typescript 2.8 ---
const conditionalTypesExample = new ConditionalTypesExample();

// --- Typescript 2.7 ---
// https://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-7.html
const constantNamedPropertiesExample = new ConstantNamedPropertiesExample();
const definiteAssignmentExample = new DefiniteAssignmentExample();
const numericSeparatorsExample = new NumericSeparatorsExample();

// --- Typescript 2.5 ---
// https://blog.mariusschulz.com/2018/01/30/typescript-2-5-optional-catch-binding
const optionalCatchClauseVariablesExample = new OptionalCatchClauseVariablesExample();

// --- Typescript 2.4 ---
// https://blog.mariusschulz.com/2017/10/27/typescript-2-4-string-enums
const stringEnumsExample = new StringEnumsExample();
// https://blog.mariusschulz.com/2017/12/01/typescript-2-4-weak-type-detection
const weakTypeDetectionExample = new WeakTypeDetectionExample();

// --- Typescript 2.3 ---
// https://blog.mariusschulz.com/2017/06/02/typescript-2-3-generic-parameter-defaults
const genericParameterDefaultsExample = new GenericParameterDefaultsExample();

// --- Typescript 2.2 ---
// https://blog.mariusschulz.com/2017/02/24/typescript-2-2-the-object-type
const objectTypeExample = new ObjectTypeExample();

// --- Typescript 2.1 ---
// Support number and symbol named properties with keyof and mapped types (Typescript 2.9)
// keyof and Lookup Types - http://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-1.html
// Partial, Readonly, Record, and Pick - http://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-1.html
const lookupTypesExample = new LookupTypesExample();

// --- Typescript 2.0 ---
const neverExample = new NeverExample();


// TuplesExample.basicTuple();
// lookupTypesExample.partialExample();
// stringEnumsExample.printEnums();
// functionPropertiesExample.basicExample();
bigIntExample.maxNumber();
optionalChainingExample.example();
nullishCoalescingExample.example();
